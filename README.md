# Summer School 2018

## Lessoners
- Dr. Gerardo Marx Chávez-Campos
- Dr. Mehmet Yüksekkaya

## Repository description
Repository for the summer school at Jade University in 2018. The repository includes and all data to develop the project analogue stethoscope and frequency analysis with Fourier transform using MATLAB.

## Course description
Biomedical signals are used to obtain crucial information about human health for diagnosis and treatment follow up. Acquiring biosignals from the human body to a computer system is called biosignal acquisition and conditioning. For signal acquisition and conditioning, appropriate electronic instrumentation should be designed. In order to extract the crucial information from the acquired data, it should be processed. 

This course is designed to be a practical introduction for all those stages. Students will design basic instrumentation circuitry to acquire a specific biosignal and learn the fundamentals of signal conditioning. Further, they will learn the essential principles of digital signal processing and will apply those principles in a practical example. After this course, students will get significant theoretical and practical skills for biosignal acquisition and processing.

## Schedule

| Date        | Course Syllabus|
| ---------- :|:--------------:|
| 1    		  | Introduction to biosignal and practical introduction to MATLAB and PCB circuit design |
| 2           | Design, implementation and tests of a practical biosignal acquisition instrumentation |
| 3	          | Fundamentals of digital signal processing to be applied for the design      |
| 4			  | Biosignal analysis and feature extraction |

## Useful links



